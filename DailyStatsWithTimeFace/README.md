A watchface with a snapshot of where you are in your daily tracking. 
It simultaneously shows time, instantaneous heart rate, number of steps out of 
the total goal number of steps, total distance, and total calories. 
How far into you are into your movement alert is shown via a progression bar 
that also changes color as you get deeper into lack of steps. An Icon tray 
indicates battery level, if there are any alarms, if there are any alerts, and 
if you are connected to your phone.

Icons are from from the Google Design repository, used as is.  Google
released the icons released under a 
CC-BY license: https://creativecommons.org/licenses/by/4.0/
Google Icons Link: https://design.google.com/icons/

